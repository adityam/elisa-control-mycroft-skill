import sys
import dbus
import glib
import os
import psutil
from traceback import print_exc
from os.path import dirname
from adapt.intent import IntentBuilder
from mycroft.skills.core import MycroftSkill, intent_handler
from mycroft.util.log import getLogger

__author__ = 'aix'

LOGGER = getLogger(__name__)

class ElisaMusicPlayerSkill(MycroftSkill):

    # The constructor of the skill, which calls MycroftSkill's constructor
    def __init__(self):
        super(ElisaMusicPlayerSkill, self).__init__(name="ElisaMusicPlayerSkill")
        self.load_data_files(dirname(__file__))
        
    @intent_handler(IntentBuilder("ElisaPlayKeywordIntent").require("ElisaPlayKeyword").build())
    def handle_internals_elisa_play_skill_intent(self, message):    
	self.speak_dialog("elisa.play")
        elisaRunning = False   

        for proc in psutil.process_iter():
            pinfo = proc.as_dict(attrs=['pid', 'name'])    
            if pinfo['name'] == 'elisa':
                elisaRunning = True
    
        if elisaRunning:
            #print('yes')
	    def runplay():
       		 bus = dbus.SessionBus()
        	 remote_object = bus.get_object("org.mpris.MediaPlayer2.elisa","/org/mpris/MediaPlayer2")
        	 remote_object.Play(dbus_interface = "org.mpris.MediaPlayer2.Player")	
	    runplay()
        
	else:
       	   def runprocandplay():
           	os.system("elisa")
           	bus = dbus.SessionBus()
           	remote_object = bus.get_object("org.mpris.MediaPlayer2.elisa","/org/mpris/MediaPlayer2")
           	remote_object.Play(dbus_interface = "org.mpris.MediaPlayer2.Player")
           runprocandplay()
   
    @intent_handler(IntentBuilder("ElisaStopKeywordIntent").require("ElisaStopKeyword").build())
    def handle_internals_elisa_stop_skill_intent(self, message):        
        bus = dbus.SessionBus()
        remote_object = bus.get_object("org.mpris.MediaPlayer2.elisa","/org/mpris/MediaPlayer2") 
        remote_object.Stop(dbus_interface = "org.mpris.MediaPlayer2.Player")
        
        self.speak_dialog("elisa.stop")
        
    @intent_handler(IntentBuilder("ElisaNextKeywordIntent").require("ElisaNextKeyword").build())
    def handle_internals_elisa_next_skill_intent(self, message):
        bus = dbus.SessionBus()
        remote_object = bus.get_object("org.mpris.MediaPlayer2.elisa","/org/mpris/MediaPlayer2") 
        remote_object.Next(dbus_interface = "org.mpris.MediaPlayer2.Player")
        
        self.speak_dialog("elisa.next")
        
    @intent_handler(IntentBuilder("ElisaPreviousKeywordIntent").require("ElisaPreviousKeyword").build())     
    def handle_internals_elisa_previous_skill_intent(self, message):
        
        bus = dbus.SessionBus()
        remote_object = bus.get_object("org.mpris.MediaPlayer2.elisa","/org/mpris/MediaPlayer2") 
        remote_object.Previous(dbus_interface = "org.mpris.MediaPlayer2.Player")
        
        self.speak_dialog("elisa.previous")     
        
    @intent_handler(IntentBuilder("ElisaPauseKeywordIntent").require("ElisaPauseKeyword").build())
    def handle_internals_elisa_pause_skill_intent(self, message):
        
        bus = dbus.SessionBus()
        remote_object = bus.get_object("org.mpris.MediaPlayer2.elisa","/org/mpris/MediaPlayer2") 
        remote_object.Pause(dbus_interface = "org.mpris.MediaPlayer2.Player")
        
        self.speak_dialog("elisa.pause")     
        
    def stop(self):
        pass

# The "create_skill()" method is used to create an instance of the skill.
# Note that it's outside the class itself.
def create_skill():
    return ElisaMusicPlayerSkill()
