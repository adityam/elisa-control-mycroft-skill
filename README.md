# elisa-control-mycroft-skill
Elisa music player control integration with Mycroft AI

#### Installation of skill:
* Download or Clone Git
* Create /opt/mycroft/skills folder if it does not exist
* Extract Downloaded Skill into a folder. "elisa-control-mycroft-skill". (Clone does not require this step)
* Copy the elisa-control-mycroft-skill folder to /opt/mycroft/skills/ folder

##### How To Use: 
###### Play Music/Song
- "Elisa play music"
- "Elisa play song"

###### Pause Music/Song
- "Elisa pause music"
- "Elisa pause song"

###### Stop Music/Song
- "Elisa stop music"
- "Elisa stop song"

###### Next Song
- "Elisa next song"

###### Previous Song
- "Elisa previous song"

## Current state

Working features:
* Play Music
* Pause Music
* Stop Music
* Next Song
* Previous Song

#### Dependency Requirements:
##### Fedora: 
- sudo dnf install dbus-python
- sudo dnf install python-psutil
- From terminal: cp -R /usr/lib64/python2.7/site-packages/dbus* /home/$USER/.virtualenvs/mycroft/lib/python2.7/site-packages/
- From terminal: cp /usr/lib64/python2.7/site-packages/_dbus* /home/$USER/.virtualenvs/mycroft/lib/python2.7/site-packages/

##### Kubuntu / KDE Neon: 
- sudo apt install python-psutil
- sudo apt install python-dbus
- From terminal: cp -R /usr/lib/python2.7/dist-packages/dbus* /home/$USER/.virtualenvs/mycroft/lib/python2.7/site-packages/
- From terminal: cp /usr/lib/python2.7/dist-packages/_dbus* /home/$USER/.virtualenvs/mycroft/lib/python2.7/site-packages/

* For other distributions:
- Python Dbus and Python Psutil package is required and copying the Python Dbus folder and lib from your system python install over to /home/$USER/.virtualenvs/mycroft/lib/python2.7/site-packages/.
